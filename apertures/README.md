# Aperture definitions

Apertures are implemented as classes. The only requirements for each type of aperture is that the following methods are implemented:

1. `is_separable()`

    This method outputs whether the integral is separable over azimuthal and elevation spatial frequencies $`u`$ and $`v`$, respectively.
2. `fourier_transform(k, z)`

    This method outputs an explicit 2D spatial Fourier transform function definition as a function of $`u`$ and $`v`$. In the case of a separable aperture, this function returns separate functions of $`u`$ and $`v`$.

3. `spat_freqs(k, z, xd, yd, nzu, nzv, dzu, dzv)`

    Given the desired number of zero-crossings in the sinc functions in $`u`$ and $`v`$ induced by the rectangular aperture extent, as well as the desired sample spacing (also in units of zero-crossings), this method outputs the corresponding spatial frequencies.

Several apertures are detailed below.

## Rectangular aperture

Defined in [RectAperture.py](RectAperture.py).

The default configuration is a rectangular aperture with a parabolic focus, defined as

```math
A(x,y) = \begin{dcases}
1, & x\in[x_0,x_1], y\in[y_0,y_1] \\
0, & \textrm{otherwise}
\end{dcases}
```

Note that the value is 1 because the derivation assumes a parabolic-focused aperture. The rectangular aperture is trivially separable in $`x`$ and $`y`$ and is thus separable in $`u`$ and $`v`$.

The Fourier transform of the aperture is a sinc function, defined as

```math
\widetilde{A}(u,v) = \left( e^{-j\pi u (x_0+x_1)} \frac{\sin\pi u(x_1-x_0)}{\pi u} \right)\left( e^{-j\pi v (y_0+y_1)} \frac{\sin\pi v(y_1-y_0)}{\pi v} \right).
```

## Infinite plane wave

Defined in [PlaneInfinite.py](PlaneInfinite.py).

The ideal infinite plane wave aperture is a useful tool for theoretical coherence analysis. A plane wave with azimuthal steering $`\alpha`$ and elevational steering $`\beta`$ is defined as

```math
T(x,y) = \exp\left(jk\left[x\sin\alpha-\frac{x^2}{2z} + y\sin\beta-\frac{y^2}{2z}\right]\right),
```

where the implicit parabolic focus is counteracted by the $`e^{-jk\mathbf{x}^T\mathbf{x}/(2z)}`$ chirp term. This aperture is separable in $`x`$ and $`y`$, and is thus separable in spatial frequencies $`u`$ and $`v`$.

The Fourier transform of this aperture in $`x`$ is

```math
T(u) = \int_{\mathbb{R}}\exp\left(j\left[kx\sin\alpha-\frac{kx^2}{2z} + 2\pi u x \right]\right) dx.
```

We can approximate this integral using the **principle of stationary phase** (thank you to M. Jakovljevic for the suggestion). The phase term is defined as

```math
\phi_x(x) = (k\sin\alpha + 2\pi u) x - \left(\frac{k}{2z}\right)x^2.
```

Because of the chirp term, the phase oscillates with respect to $`x`$ as

```math
\frac{d\phi_x}{d x} = k\sin\alpha + 2\pi u - \frac{kx}{z}
```

We can determine the $`x`$ for which the phase is stationary (i.e. $`\frac{d\phi_x}{d x}=0`$) as

```math
x_0 = z\left(\sin\alpha + \frac{2\pi u}{k}\right)
```

When integrating over $`x\in\mathbb{R}`$, only the stationary phase $`\phi_x(x_0)`$ is aperiodic. Thus, we approximate the Fourier transform as

```math
T(u)\approx \exp\left( j\left[kx_0\sin\alpha - \frac{kx_0^2}{2z}+2\pi ux_0\right] \right).
```

The Fourier transform can be found with respect to $`y`$ using the same procedure.In the code, we also provide the option for a parabolic focus in either dimension by specifying the steering angle as `np.nan`, e.g., to mimic a physical lens.

## Plane wave aperture

Defined in [PlaneAperture.py](PlaneAperture.py).

In practical ultrasound imaging, plane waves have finite extent as bounded by the aperture. Here, we define a plane wave *aperture* class, which is derived from the **RectAperture** class. Observe that a plane wave aperture is the product of a rectangular aperture and an infinite plane wave. Therefore, the Fourier transform is the convolution of the Fourier transforms of infinite plane waves and rectangular apertures.
