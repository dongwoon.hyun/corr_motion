import numpy as np


class PlaneInfinite:
    def __init__(self, xdir=0, ydir=0):
        # Run plane-wave-specific components
        self.xdir = xdir  # Azimuthal angle
        self.ydir = ydir  # Elevation angle

    ## Make 2D Fourier transformed aperture functions for plane wave transmit.
    def fourier_transform(self, k, z):
        # Use the principle of stationary phase, weighted by window function
        def fu(u):
            # Find the value of x where the phase is stationary for all u
            x0 = z * (np.sin(self.xdir) + 2 * np.pi * u / k)
            phi0 = x0 * (k * np.sin(self.xdir) + 2 * np.pi * u - k * x0 / z / 2)
            return np.exp(-1j * phi0)

        def fv(v):
            # Find the value of y where the phase is stationary for all v
            y0 = z * (np.sin(self.ydir) + 2 * np.pi * v / k)
            phi0 = y0 * (k * np.sin(self.ydir) + 2 * np.pi * v - k * y0 / z / 2)
            return np.exp(-1j * phi0)

        # Return the Fourier transforms as functions of spatial frequencies u, v
        return fu, fv

    ## Get maximum spatial frequencies and minimum increments for integration over u, v
    def spat_freqs(self, k, z, xd, yd, nzu, nzv, dzu, dzv):
        # Return default values; let integration limits be selected by other apertures
        return 0, 0, np.inf, np.inf

