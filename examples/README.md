# Example usage of corr_motion

**corr_motion** can be used to recover classical results from ultrasound coherence theory derived for stationary scatterers, and also extends ultrasound coherence theory to the case of moving scatterers.

---

## Spatial coherence of stationary scatterers

[coherence_spatial.py](coherence_spatial.py) predicts the spatial coherence for stationary scatterers as a function of receive aperture spacing. **The spatial coherence function depends on the width of the transmit aperture function.** This result is consistent with results by [Mallart and Fink, JASA 1991](https://asa.scitation.org/doi/10.1121/1.401867) and [Walker and Trahey, JASA 1997](https://asa.scitation.org/doi/10.1121/1.418235), among others.

![coherence_spatial](images/coherence_spatial.png)

---

## Angular coherence of stationary scatterers, infinite aperture

[coherence_angular_ideal.py](coherence_angular_ideal.py) predicts the angular coherence for stationary scatterers as a function of angular spacing. This example assumes idealized *infinite* plane wave apertures. **The angular coherence function depends on the width of the receive aperture function**, agreeing with the results of [Li and Dahl, JASA 2019](https://asa.scitation.org/doi/full/10.1121/1.4976960).

![coherence_angular_ideal](images/coherence_angular_ideal.png)

---

## Angular coherence of stationary scatterers, finite aperture

[coherence_angular.py](coherence_angular.py) predicts the angular coherence for stationary scatterers as a function of angular spacing. This example uses realistic *finite* plane wave apertures. We observe that **the angular coherence of finite apertures is almost identical to that of ideal plane waves.**

![coherence_angular](images/coherence_angular.png)

---

## Phase-shift estimation with focused apertures

[motion_rect.py](motion_rect.py) predicts the impact of axial and azimuthal scatterer motion on phase-shift estimation. This example uses focused transmit and receive apertures from two subsequent pulse-echo events. The axial motion is measured as

```math
\hat{z}_d = \frac{c}{4\pi f_c}\angle\Gamma_{12}.
```

The jitter is estimated using the equation for the variance in phase-shift of two decorrelated speckle signals:

```math
\sigma^2_{\angle\Gamma_{12}} = \frac{\pi^2}{3}-\pi\arcsin \mu + (\arcsin \mu)^2 - \frac{1}{2}\mathrm{Li}_2(\mu^2),
```

where jitter is the square root of $`\sigma^2_{\angle\Gamma_{12}}`$.

The theory predicts that **only axial motion affects the mean phase-shift estimate, and that both azimuthal and axial motion increase jitter.** Aliasing is also observed when the phase-shift magnitude is greater than $`\pi`$.

![motion_rect](images/motion_rect.png)

---

## Ultrafast Doppler with plane wave synthetic transmit apertures

[motion_plane.py](motion_plane.py) predicts the impact of axial and azimuthal scatterer motion on phase-shift estimation when using a plane wave synthetic transmit aperture. As with the focused apertures, azimuthal motion is observed to increase jitter, whereas axial motion changes the estimate (as expected) while also increasing jitter. Aliasing occurs when the phase-shift magnitude is greater than $`\pi`$. **These results demonstrate that motion estimation with synthetic apertures is robust to scatterer motion.** (Experiment adapted from [Hyun and Dahl, JASA 2020](https://asa.scitation.org/doi/10.1121/10.0000809).)

![motion_plane](images/motion_plane.png)

---

## Sequential versus interleaved ultrafast Doppler

[motion_interleaved.py](motion_interleaved.py) reproduces a recent result by [Jensen, TUFFC 2019](https://ieeexplore.ieee.org/document/8672181) that demonstrates that interleaved synthetic apertures can estimate larger displacements by effectively increasing the aliasing limit $`N`$-fold as compared to standard synthetic apertures. If the $`i`$-th synthetic pulse-echo signal is the sum of components $`a_i, b_i, c_i, \ldots`$, a traditional *sequential* acquisition is obtained as

```math
\textrm{Sequential:}\quad a_1, b_1, c_1, \ldots, a_2, b_2, c_2, \ldots
```

Jensen proposes the following *interleaved* acquisition:

```math
\textrm{Interleaved:}\quad a_1, a_2, b_1, b_2, c_1, c_2, \ldots\qquad
```

**The theory confirms that interleaved acquisitions increase the aliasing limit, and further shows that the jitter is expected to be comparable to sequential acquisitions.** (Experiment adapted from [Hyun and Dahl, JASA 2020](https://asa.scitation.org/doi/10.1121/10.0000809).)

![motion_interleaved](images/motion_interleaved.png)

---

## Spatial coherence function of synthetic aperture with scatterer motion

[motion_coherence_spatial.py](motion_coherence_spatial.py) predicts the impact of azimuthal scatterer motion on the spatial coherence function when using a synthetic transmit aperture. The synthetic aperture is composed of four disjoint focused subapertures acquired sequentially. **Small motions (<100um per synthetic aperture) do not affect the spatial coherence function, but larger motions (1mm, 10mm) are observed to cause decorrelation.** (Experiment adapted from [Hyun and Dahl, JASA 2020](https://asa.scitation.org/doi/10.1121/10.0000809).)

![motion_coherence_spatial](images/motion_coherence_spatial.png)
